from manimlib import *
from ..imports import *

NUMBER_LINE_CONFIG = {
    "include_ticks": False,
    "include_tip": False
}

AXES_CONFIG = {
    "x_range": np.array([0, 3, 1.0]),
    "y_range": np.array([0, 6, 1.0]),
    "z_range": np.array([0, 3, 1.0]),
    "x_axis_config": NUMBER_LINE_CONFIG,
    "y_axis_config": NUMBER_LINE_CONFIG,
    "z_axis_config": {
        "include_ticks": False,
        "include_tip": False,
        # "stroke_width": 30
    },
}

class PraxisScene(Scene):
    def rotate_about_axes(self, mob, axes):
        mob.rotate(-90*DEGREES,about_point=ORIGIN)
        mob.rotate(-90*DEGREES,axis=LEFT)
        mob.rotate(-10*DEGREES,axis=UP)

    def get_axes(self, axes_config):
        axes = ThreeDAxes(**axes_config)
        axes.x_axis.set_color(RED)
        axes.y_axis.set_color(BLUE)
        axes.z_axis.set_color(GREEN)
        axes.rotate(-90*DEGREES,about_point=axes.coords_to_point(0,0))
        axes.rotate(-90*DEGREES,axis=axes.y_axis.get_vector())
        axes.rotate(-10*DEGREES,axis=axes.z_axis.get_vector())
        # axes.rotate(20*DEGREES,axis=axes.y_axis.get_vector())
        # axes.z_axis.rotate(280*DEGREES,axis=axes.z_axis.get_vector())
        axes.shift(UP*3+LEFT)

        x_axis = Line3D(axes.x_axis.get_start(),axes.x_axis.get_end(),color=RED)
        y_axis = Line3D(axes.y_axis.get_start(),axes.y_axis.get_end(),color=BLUE)
        z_axis = Line3D(axes.z_axis.get_start(),axes.z_axis.get_end(),color=GREEN)
        return axes,SGroup(x_axis,y_axis,z_axis)

class MR2(PraxisScene):
    def construct(self):
        axes, axes3d = self.get_axes(AXES_CONFIG)
        xa, ya, za = axes3d

        remolque = Prism(dimensions=[0.4,1,0.4],color=GRAY)
        self.rotate_about_axes(remolque,axes)
        remolque.move_to(axes.coords_to_point(0,0))

        Y_VECTOR = axes.y_axis.get_unit_vector()
        X_VECTOR = axes.x_axis.get_unit_vector()

        grua = Line(remolque.get_top(),remolque.get_top()+UP-Y_VECTOR)

        def update_grua(mob,alpha):
            new_grua = Line(remolque.get_top(),remolque.get_top()+UP-Y_VECTOR)
            new_grua.rotate(
                120*DEGREES*alpha,
                axis=-X_VECTOR,
                about_point=remolque.get_top()
            )
            grua.become(new_grua)
            grua.fade()

        GRUA_CONFIG = {"color": TEAL}

        s_grua = Line3D(grua.get_start(),grua.get_end(),**GRUA_CONFIG)
        s_grua.add_updater(
            lambda mob: mob.become(Line3D(grua.get_start(),grua.get_end(),**GRUA_CONFIG))
        )
        # grua.add_updater(update_grua)


        self.add(
            axes3d,
            remolque,
            grua,
            s_grua,
        )

        self.play(
            remolque.animate.shift(Y_VECTOR*3),
            UpdateFromAlphaFunc(grua,update_grua),
            rate_func=linear,
            run_time=5
        )