from manimlib import *
from ..imports import *

NUMBER_LINE_CONFIG = {
    "include_ticks": False,
    "include_tip": False
}

AXES_CONFIG = {
    "x_range": np.array([0, 3, 1.0]),
    "y_range": np.array([0, 3, 1.0]),
    "z_range": np.array([0, 3, 1.0]),
    "x_axis_config": NUMBER_LINE_CONFIG,
    "y_axis_config": NUMBER_LINE_CONFIG,
    "z_axis_config": {
        "include_ticks": False,
        "include_tip": False,
        # "stroke_width": 30
    },
}

class PraxisScene(Scene):
    def rotate_about_axes(self, mob, axes):
        mob.rotate(-90*DEGREES,about_point=ORIGIN)
        mob.rotate(-90*DEGREES,axis=LEFT)
        mob.rotate(-10*DEGREES,axis=UP)

    def get_axes(self, axes_config):
        axes = ThreeDAxes(**axes_config)
        axes.x_axis.set_color(RED)
        axes.y_axis.set_color(BLUE)
        axes.z_axis.set_color(GREEN)
        axes.rotate(-90*DEGREES,about_point=axes.coords_to_point(0,0))
        axes.rotate(-90*DEGREES,axis=axes.y_axis.get_vector())
        axes.rotate(-10*DEGREES,axis=axes.z_axis.get_vector())
        # axes.rotate(20*DEGREES,axis=axes.y_axis.get_vector())
        # axes.z_axis.rotate(280*DEGREES,axis=axes.z_axis.get_vector())
        axes.shift(UP*2)

        x_axis = Line3D(axes.x_axis.get_start(),axes.x_axis.get_end(),color=RED)
        y_axis = Line3D(axes.y_axis.get_start(),axes.y_axis.get_end(),color=BLUE)
        z_axis = Line3D(axes.z_axis.get_start(),axes.z_axis.get_end(),color=GREEN)
        return axes,SGroup(x_axis,y_axis,z_axis)

class MR3(PraxisScene):
    CONFIG = {
        "cylinder_1_config": {
            "height": 0.3,
            "radius": 0.3,
        },
        "cylinder_2_config": {
            "height": 0.5,
            "radius": 1
        },
    }
    def get_cylinders(self, axes):
        c1 = Cylinder(**self.cylinder_1_config)
        c2 = Cylinder(**self.cylinder_2_config)
        disk = Disk3D(radius=self.cylinder_2_config["radius"]*1.005)
        for mob in [c1,c2,disk]:
            self.rotate_about_axes(mob,axes)
        c1.next_to(axes.coords_to_point(0,0),UP,buff=0)
        c2.next_to(axes.coords_to_point(0,0),UP,buff=self.cylinder_1_config["height"])
        disk.next_to(
            axes.coords_to_point(0,0),UP,
            self.cylinder_1_config["height"]+self.cylinder_2_config["height"]
        )
        
        return SGroup(c1,c2,disk)

    def construct(self):
        axes, axes3d = self.get_axes(AXES_CONFIG)
        xa, ya, za = axes3d

        cylinders = self.get_cylinders(axes)
        prism = Prism(dimensions=[1.5,1.5,0.4],color=GRAY)
        self.rotate_about_axes(prism,axes)
        prism.next_to(axes.coords_to_point(0,0),DOWN,buff=0)

        Y_VECTOR = axes.y_axis.get_unit_vector()
        X_VECTOR = axes.x_axis.get_unit_vector()

        def get_grua(angle):
            start = cylinders[-1].get_center()
            end = cylinders[-1].get_center() + UP * 1.5
            line = Line(start,end)
            line.rotate(PI/2-angle,axis=-X_VECTOR,about_point=start)
            return line

        def update_grua(mob,alpha):
            line = get_grua(30*DEGREES)
            line.rotate(80*DEGREES*alpha,axis=X_VECTOR,about_point=mob.get_start())
            line.rotate(120*DEGREES*alpha,axis=UP,about_point=mob.get_start())
            mob.become(line)

        grua = get_grua(30*DEGREES)

        GRUA_CONFIG = {"color": TEAL}
        s_grua = Line3D(grua.get_start(),grua.get_end(),**GRUA_CONFIG)
        s_grua.add_updater(
            lambda mob: mob.become(Line3D(grua.get_start(),grua.get_end(),**GRUA_CONFIG))
        )

        plane = Square3D(side_length=2)
        self.rotate_about_axes(plane,axes)
        plane.next_to(axes.coords_to_point(0,0),DOWN,buff=0)
        plane.rotate(PI/2,axis=Y_VECTOR)
        plane.fade(0.6)
        plane.shift(UP)
        plane.save_state()
        def update_plane(mob,alpha):
            mob.restore()
            mob.rotate(120*DEGREES*alpha,axis=UP)

        self.add(
            axes3d,
            cylinders,
            prism,
            grua,
            s_grua,
            plane
        )

        self.play(
            UpdateFromAlphaFunc(grua,update_grua),
            UpdateFromAlphaFunc(plane,update_plane),
            run_time=8,
            rate_func=linear,
        )
        self.wait(0.1)
