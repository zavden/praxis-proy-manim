# Este archivo debe estar en [virtual_env_manim]\Lib\site-packages\manim\utils\praxis_template.py
# Debes reemplazar virtual_env_manim con el nombre de tu entorno virtual de manim

# This file must be located in [virtual_env_manim]\Lib\site-packages\manim\utils\praxis_template.py
# You should replace virtual_env_manim with the name of your manim virtual environment

from manim import *

font_temp = TexFontTemplates.ecf_augie                                              # Uso de la fuente ECF_AUGIE // Use of ECF_AUGIE font

font_temp.add_to_preamble(r"\DeclareMathOperator{\tg}{tg}")                         # Uso de tg en vez de tan // Use of tg instead of tan
font_temp.add_to_preamble(r"\DeclareMathOperator{\arctg}{arctg}")                   # Uso de arctg en vez de arctan // Use of arctg instead of arctan
font_temp.add_to_preamble(r"\DeclareMathOperator{\argtgh}{argtgh}")  

font_temp.add_to_preamble(r"\newcommand*{\myfont}{\fontfamily{phv}\selectfont}")    # Cambiar el número 7 por uno más visibile // Replacing 7
font_temp.add_to_preamble(r"\DeclareTextFontCommand{\textmyfont}{\myfont}")
font_temp.add_to_preamble(r"\newcommand*{\myfonto}{\fontfamily{pag}\selectfont}")    
font_temp.add_to_preamble(r"\DeclareTextFontCommand{\textmyfonto}{\myfonto}")
font_temp.add_to_preamble(r"""
\usepackage{stackengine,} %
\newcommand{\7}{\ensuremath{\mathord{\stackinset{c}{0.1ex}{c}{-1pt}{-}{\textmyfont{7}}}}}""")
font_temp.add_to_preamble(r"\newcommand{\1}{\textmyfonto{1}}")                       # Cambiar el número 1 por uno más visible // Replacing 1
font_temp.add_to_preamble(r"\newcommand{\bp}{\textmyfont{b}}")                       # Cambiar la b por una más visible // Replacing b
font_temp.add_to_preamble(r"""
\usepackage{stackengine,} %
\newcommand{\z}{\ensuremath{\mathord{\stackinset{c}{0ex}{c}{-1pt}{\textmyfont{-}}{\textmyfont{z}}}}}""")