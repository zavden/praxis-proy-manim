from manimlib import *
from ..imports import *

NUMBER_LINE_CONFIG = {
    "include_ticks": False,
    "include_tip": False
}

AXES_CONFIG = {
    "x_range": np.array([0, 3, 1.0]),
    "y_range": np.array([0, 3, 1.0]),
    "z_range": np.array([0, 3, 1.0]),
    "x_axis_config": NUMBER_LINE_CONFIG,
    "y_axis_config": NUMBER_LINE_CONFIG,
    "z_axis_config": {
        "include_ticks": False,
        "include_tip": False,
        # "stroke_width": 30
    },
}

class PraxisScene(Scene):
    def rotate_about_axes(self, mob, axes):
        mob.rotate(-90*DEGREES,about_point=ORIGIN)
        mob.rotate(-90*DEGREES,axis=LEFT)
        mob.rotate(-10*DEGREES,axis=UP)

    def get_axes(self, axes_config):
        axes = ThreeDAxes(**axes_config)
        axes.x_axis.set_color(RED)
        axes.y_axis.set_color(BLUE)
        axes.z_axis.set_color(GREEN)
        axes.rotate(-90*DEGREES,about_point=axes.coords_to_point(0,0))
        axes.rotate(-90*DEGREES,axis=axes.y_axis.get_vector())
        axes.rotate(-10*DEGREES,axis=axes.z_axis.get_vector())
        # axes.rotate(20*DEGREES,axis=axes.y_axis.get_vector())
        # axes.z_axis.rotate(280*DEGREES,axis=axes.z_axis.get_vector())
        axes.shift(UP*2)

        x_axis = Line3D(axes.x_axis.get_start(),axes.x_axis.get_end(),color=RED)
        y_axis = Line3D(axes.y_axis.get_start(),axes.y_axis.get_end(),color=BLUE)
        z_axis = Line3D(axes.z_axis.get_start(),axes.z_axis.get_end(),color=GREEN)
        return axes,SGroup(x_axis,y_axis,z_axis)

class MR1(PraxisScene):
    def construct(self):
        axes, axes3d = self.get_axes(AXES_CONFIG)
        xa, ya, za = axes3d

        CYLINDER_HEIGHT = 0.2
        CYLINDER_RADIUS = 2
        cylinder = Cylinder(radius=CYLINDER_RADIUS,height=CYLINDER_HEIGHT)
        cylinder_top = Disk3D(radius=CYLINDER_RADIUS*1.01)
        self.rotate_about_axes(cylinder,axes)
        self.rotate_about_axes(cylinder_top,axes)
        cylinder.next_to(axes.coords_to_point(0,0),DOWN,buff=0)
        cylinder_top.next_to(axes.coords_to_point(0,0),UP,buff=0)
        cylinder_grp = SGroup(cylinder,cylinder_top).set_color(GRAY)

        guide_line = axes.z_axis
        guide_line.set_color(GRAY)
        guide_line.shift(axes.y_axis.get_unit_vector()*2)
        guide_line.set_height(2,
            stretch=True,
            about_point=axes.coords_to_point(0,CYLINDER_RADIUS)
        )
        guide_line.shift(LEFT*0.05)
        s_guide_line = Line3D(guide_line.get_start(),guide_line.get_end())
        guide_line.shift(UP*0.05)
        # guide_line.shift(UP*CYLINDER_HEIGHT)
        guide_line.set_color(TEAL)
        guide_line.fade(1)

        # s_guide_line.set_color(TEAL)

        mob_grp = SGroup(*cylinder_grp,s_guide_line)

        dot3d = Sphere(radius=0.1,color=RED)
        dot3d.move_to(guide_line.get_start())

        trace = TracedPath(dot3d.get_center)

        self.add(
            # axes,
            axes3d,
            cylinder_grp,
            guide_line,
            dot3d,
            trace
        )

        self.play(
            Rotate(mob_grp,angle=TAU*2,axis=UP,run_time=12,rate_func=linear),
            MaintainPositionRelativeTo(guide_line,s_guide_line),
            MoveAlongPath(dot3d,guide_line,run_time=12,rate_func=linear)
        )
        self.wait(0.1)
